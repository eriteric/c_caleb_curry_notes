#include <stdio.h>
#include <stdbool.h>

int main()
{
	int input = 23;
	bool isPrime = true;
	
	// for any number between 1 and itself, if it's divisible evenly, then it is NOT prime
	for(int i = input - 1; i > 1; i--)
	{
		printf("%d ", i);
		if(input % i == 0)
		{
			isPrime = false;
		}
	}
	if (isPrime)
	{
		//output
		printf("\nYea boiiiii it's Prime!\n");
	} else
	{
		printf("\nOh nooooooo, it aint prime!\n");
	}
	return 0;
}
