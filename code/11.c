#include <stdio.h>

//calculate the area of a circle

int main()
{
	int radius; // the distance from the outside to center of the cirlce

	printf("Please enter a radius: ");
	scanf("%i", &radius); // scanf = get input from the console
	//& prefixed to radius = address-of operator

	printf("The given radius is: %i\n", radius);

	return 0;
}
