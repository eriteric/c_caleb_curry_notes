#include <stdio.h>
#include <stdbool.h>

int main()
{
	int slices = 17;
	int people = 2;
	// two ways to fix this
	// 1. add `.0` to a constant
	// 2. explicit type casting
	double halfThePizza = slices / (double) people;
	printf("%f\n", halfThePizza);

	double c = 25/2 * 2;
	double d = 25/2 * 2.0;

	printf("c: %f\n", c);
	printf("d: %f\n", d);
	return 0;
}

