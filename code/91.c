#include <stdio.h>

int factorial(int number)
{
    int factorial = 1;
    for(int i = number; i > 1; i--)
    {
        factorial *= i;
        // factorial = factorial * i;
    }
    return factorial;
}

int main()
{
    int fact = factorial(5);
    printf("%d ", fact);
    printf("%d ", factorial(19)); // can put function directly instead of assigning to a variable first.
    return 0;
}
