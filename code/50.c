// the user has to guess a number from 0-5
// output whether or not the person is correct

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	int maxValue = 5;
	//Pseudo-Random Number Generator (seeded from clock)
	// we are telling srand where to get a value from:
	srand(time(NULL)); //srand = seed rand?
	int randomNumber = rand() % maxValue + 1; //rand uses srand?
	// why plus one? - it allows for a remainder of 0-5 if it is one over

	// printf("%d\n", randomNumber);

	printf("Guess a number 0 - %d: \n", maxValue);
	int input;
	scanf("%d", &input);

	if(input == randomNumber)
	{
		printf("You Win!\n");
	} else
	{
		printf("You don't win! Try again!\n");
	}
}
