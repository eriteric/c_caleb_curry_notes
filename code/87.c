#include <stdio.h>
#include <string.h>

int main()
{
    printf("What is your favorite food?: ");
    char favFood[50];
    scanf("%49s", favFood); //don't have to use address-of operator because of decay...when we pass in the array it decays to a pointer.
    printf("%s\n", favFood);

    // int charCount = 0;
    int charCount = strlen(favFood);

    // while(favFood[charCount] != '\0')
    // {
    //     charCount++;
    // }
    printf("The character count is %d\n", charCount);
	return 0;
}
