#include <stdio.h>

//calculate the area of a circle

int main()
{
	int radius; // the distance from the outside to center of the circle

	printf("Please enter a radius: ");
	scanf("%i", &radius); // scanf = get input from the console
	//& prefixed to radius = address-of operator
	// int area = 3.14159 * (radius * radius); // logical error (it won't = an int, aka a whole number)
	double area = 3.14159 * (radius * radius);

	printf("The area of a circle with %i radius is %f\n", radius, area);

	return 0;
}
