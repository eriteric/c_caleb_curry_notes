#include <stdio.h>
#include <math.h>

int main()
{
	double input1;
	double input2;
	
	printf("This app will calculate the hypotenuse of a right triangle. \n");
	printf("Enter two doubles: \n");
	scanf("%lf", &input1); // %lf is a double in scanf() (stand for long float?)
// this line break apparently is necessary so the next scanf() isn't skipped over...?

	printf("Enter second double: \n");
	scanf("%lf", &input2);
	
	double result = sqrt((input1 * input1) + (input2 * input2));
	// why do we need the sqrt() function if we are writing out the whole equation?
	// double result = (input1 * input1) + (input2 * input2);
	// not the same result...something to do with squaring?
	// yes.. sqrt is square root only...has nothing to do with pythagorean therom. So we are finding the square root A sqaured plus B squared... but that's not how the theorom goes...
	// In the pythagorean theorem, if A and B are known, then C can be found by getting the square root of A squared + B squared. I don't know why this works but it does..
	// it just does, is kind of the basis of theorem..

	printf("Result: %f\n", result);

	return 0;
}
