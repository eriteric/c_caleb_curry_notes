# C Operators

In this expression:
`5 + 5`
- 5 is an operand
- the plus sign is an operator
- "the operator works on the operands"

## Classification of operators:
- Unary (1 operand)
- Binary (2 operands)
- Ternary (3 operands)

## Grouping by use (or function)
- Arithmetic operators
- Logical operators
- Assignment operators
- Relational operators

Arithmetic:
`=` - assignment operator, gives a value to a variable
`+` - addition
`-` - subtraction
`*` - multiplication
`/` - division




