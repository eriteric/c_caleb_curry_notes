# Function Arguments, Parameters, Return Statement

Every function we create needs a name. (c doesn't have anonymous functions?). 

Parameters go in the parenthesis:
`fname(int n)`

If the return type will be an integer, then declare the data type with the declaration of the function:
`int fname(int n)`

The "body" of the function is in the curly braces: `{ }`

```
int fname(int n)
{
    // code
    return n;
}
```
return type

we return because when calling the function we want it to return something to us.

return statement

## invoking a function
`fname(4)` 4 gets used as "n"
4 here is an Argument.
Argument is the actual data we pass to the function when invoking, which the function will then use as its parameter.

functions return one thing; to return multiple things you would either change values at pointers (where they're pointing to) or encapsulate them into a struct.
