# When to use switch over if()

## Switch
* Good for Integer and Character Data Type
* No ranges
* Limited option

Why not just use if statements? There is no solid reason.
Switch statements may be more clear in certain situations.
A user menu is a good candidate for switch.

## if()
* For use with comparison operators


