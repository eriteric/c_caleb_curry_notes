# ASCII and int conversion
What value is shown depends on how we tell the computer to interpret the data.

You can simply change the conversion character to display a letter as its equivalent number etc. like on asciitable.com

`%i` - display character as an integer
`%d` is the same as an integer in printf
`%c` - display character as a character
you can also take an integer and display a character...

In printf, %d and %i are the same; a signed decimal integer.
In scanf, %d and %i also means signed integer but %i interprets the input as hexadecimal if preceded by `0x` and octal if preceded by `0` and otherwise interprets the input as decimal.

