# Assignment Operators

`+=` Plus Equals Operator, a shorthand for incrementing larger than one.

`-=` Minus Equals Operator

`*=` Multiply Equals Operator, multiply value and assign it back to the value

`/=` Division Equals Operator, divide value and assign it back to the value

`%=` Modulus Equals Operator, find remainder and assign it back to the value


