# Format character for float and double


## Conversion characters
* `%f` - Decimal notation
* `%e` - scientific notation
* `%g` - allows computer to decide

depends on size of exponent (exponent = how many times it is multiplied by 10)

* `< -4` = scientific notation
* `> 5` = scientific notation

otherise will use decimal notation

// when this is run, the console will display a + in scientific notations, this is just explicitly stating it is positive, and not a negative number

## scanf
* scanf() a double OR a scientific notation = `%lf`
* printf() is different, must use `%f` for decimal OR %e for scientific notation, as noted above.
* aka... Floats in printf() get promoted to doubles
* But scanf() works differently because it uses the address-of operator. The data type of the variable wouldn't change.
