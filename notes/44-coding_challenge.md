# Coding Challenge
## Instructions
Create a program that takes 2 doubles as inputs, and uses those to calculate the hypotenuse of a right triangle.

You can find this with the pythagorean therom
a squared + b squared = c squared

there is a square root function we can use in C:
`sqrt()`
find how to include and use it

## Notes
* The required header is: `#include <math.h>`
* Two scanf() in a row is not working... had to do a line break in order for scanf() not to be skipped over.
* Compiling with math.h included not working...had to add "-lm" to the end of the gcc compile command...don't know why that works...

