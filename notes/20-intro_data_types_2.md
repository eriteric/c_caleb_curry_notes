# Data Types 2

## Arrays
store multiple things in a group

## Strings
A double quoted sequence of characters
`"I like cats"`	- a string with 11 characters
Strings are actually just an array of characters
At the end of strings, you have to put a null character
The null character is backslash and zero
`"I like cats\0"`
Because of the null character, the array that strings are in have to have an additional character allotment.
Everything besides the null character is called the elements.

## Constant
A value
`int cats = 98;`
usually a constant is something you can write down...
3 different types of constants that are all similar:
'3' = char (because it has single quotes)
3 = int (because it has no quotes)
"3" = string (because it has double quotes)
C will interpret data for you depending on how it written.
another example: 
120.4 = a double constant (because it has a decimal)
120.4F = a float constant (because it is affixed with F)
