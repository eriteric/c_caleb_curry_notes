# Intro to Arrays

Array is a collection of data stored into one variable.
In C, use the data type and square brackets. (this is different in javascript etc where the type of bracket indicates the data type)

Arrays in C are statically sized, size cannot change. And this number is declared in the initialization.

## Integer Array
`int myGrades[5]` <--initialize and declare an integer array with a size of 5
A size of 5 means that myGrades[0] - myGrades[4] will be able to be accessed. Do not attempt to access myGrades[5] as it will result in a memory error.
It can be easy to accidentally access parts of arrays that don't exist when doing loops, for example - so be careful.



