# C Basics part 5

## Data type arithmetic

dividing an integer by an integer will result in an integer (c assumes you want an integer)
Storing it in a double just adds lots of zeros after the decimal point, effectively :)
in other words, the calculation is done before the variable is stored!

If hardcoding a number in an expression, you can just make C think it is a double by adding ".0" to the end
OR you can typecast...

## Typecasting
Prefix the number/variable with a type of variable. Put the type in parenthesis, followed by a space.
`(double) var`
This will let C know to treat this variable as a double.

Be careful with typecasting - be sure you are casting the correct number, and not a parenthetical group etc.


