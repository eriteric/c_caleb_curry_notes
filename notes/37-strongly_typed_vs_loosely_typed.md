# Strongly Typed vs Loosely Typed Languages

C is strongly typed
Strongly typed means explicitly declared data types.
Once declared it is always that type.

int = integer
`int x = 5`

We can cast values into new variables, like turning an int into a double, but the original variable/value will always be int.

## Strings
Strings are sequences of characters
concatenation is merging 2 strings
Example:
"5" + "5"
would that equal "10"
or would it equal "55"? (<-- concatenation)
A strongly typed language prevents confusion like this because of data types.


## Loosely Typed Language
Example:
`1/3`
In C this = 0 because 1 and 3 are both integers
In loosely typed languages this would probably = 0.33333

Javascript is a loosely typed language

### Duck typing 
(looks like a duck, talks like a duck...)
aka functions expecting a certain type of data but getting another..


