# How to use the Type Cast Operator

```
int slices = 17;
// two ways to fix this
// 1. add `.0` to a constant (not to a variable)
// 2. explicit type casting
double halfThePizza = slices / 2;
```

## Explicit Type Casting

Just put a "cast" before a variable:
`(double) people` or no space is fine: `(double)people`

Casting does not affect the original variable, people is still 2. 

Be careful about operator precedence when type casting. Ask: am I type casting the whole expression or just an operand?
See operator precedence for type casting also.

Just because one number in an expression has a `.0` at the end, doesn't mean the result will be a double and not an int, walk through the problem in your head and see what the result will be and where typing a .0 will matter..


