# Do While Loop

Will execute always at least once.
Example: input validation
Ask once, then if the value doesn't match the comparison keep asking until the correct value is entered.

## Syntax
```
//initialization
do
{
    //code
    //update
} while ( /* comparison */ );
```
