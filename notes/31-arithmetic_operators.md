# Arithmetic Operators

Order of operations is a math term, the computer science equivalent is "operator precedence".

You don't need to write the operator precedence usually because the computer can work it out, but it is helpful in understanding your code.

# Modulus operator
`%`
