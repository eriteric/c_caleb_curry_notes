# Logical Operator Precedence

`true || y && z`
this is an OR and an AND logical operator, which happens first?
OR: `(true || y) && z`
AND: `true || (y && z)`
With boolean simplification, we can look at a truth table and see that one part of the expression isn't necessary so it can be removed.

A | B | A && B
T | T | T
T | F | F

# Precedence
1. `!`
2. `&&`
3. `||`

`!true || y && z`
true is negated first, to false so we have:
`y && z`
