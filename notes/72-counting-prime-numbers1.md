# Counting Prime Numbers pt. 1

Real world problems usually have very little to do with interpreting math as code. But it does happen sometimes.

9 = `3*3`
7 = no whole numbers we can multiply to get 7, so it is a prime number.

If we give a function the number 25, it will list all the prime numbers from 25 down to 0, and give the total number of prime numbers. 
`2, 3, 5, 7, 11, 13, 17, 19, 23`

# Edge cases
Comes up with software testing. Example: Highest and lowest number we're going to be working with. Users may disagree that one is or isn't a prime number.

# Pseudo Code
Code that has no rules on syntax. A way  for people to write code for display in a group usually, so that we aren't worried about syntax, just the logic.

```
for (; input >=2; input--)
{
	// figure out if prime
	if num is prime
		print all prime numbers
		numPrime++;
}
```
