# Passing by Value vs Pointer

In C, everything is passed by value.
`doubleValue(int x)`
int x is a parameter

when we use the function:
```
int x = 10;
doubleValue(x);
```
x here is an argument. The value of x gets copied into the function. (there's actually 2 separate variables)

If x is changed elsewhere it wouldn't affect the variable created when passing x into the function (it's a separate value, not a reference to the value).

If we wanted to change the value of x, we'd have to return an integer (declare doubleValue() as an int), then return x = x * 2; 

## Passing pointers
Since we're working with pointers we don't need to return a value. So now we can use a void function, which means there's no return type.

```
void doubleValue(int *x);
```
and then when calling the function we would pass the address-of a variable

```
int x = 10;
doubleValue(&x);
```
So, any change we make to x this way will automatically be reflected.

so this is why scanf() takes an address-of variable...
