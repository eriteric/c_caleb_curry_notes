# Ternary (Conditional) Operator

Essentially another way to do if statements.
Very suitable for small-sized if statments.

Basic syntax is:
`Question ? true : false;`

Real world example:
`money > cost ? printf("true") : printf("false");`
