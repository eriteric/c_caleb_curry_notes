# Multidimensional Arrays

arrays up to this point were single dimensional arrays
Multidimensional arrays are essentially arrays of arrays (arrays containing arrays data type)

E.g.
```
0 based table:
_________________
|   |   |   |   | <--- studentGrades[0] (whole row)
|   |   |   |   |
|   |   |   |   |
|   |   | x |   |
|   |   |   |   |

x = studentGrades[3][2]

```
